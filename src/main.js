define(["jquery","toaster","eventHandler/EventHandler", "Menu", "connection/Connection", "DateTimePicker", "BreakpointSection", "Graphic","ProgressBar","eventHandler/InitialConfigurationModal", "WebSocketClient", "SimulationState","jquery-ui", "timepicker", "bootstrap", "highcharts","highcharts-module" ],

function main($,toaster,  eventHandler, menu, connection, datetime, breakpointSection, Graphic, progressbar, modalInitialConfiguration, WebSocketClient, SimulationState) {

    const REMOTE_DIRECTION = "localhost/simulation";


    connection.init(REMOTE_DIRECTION);
    modalInitialConfiguration.init().show().done(function () {
        WebSocketClient.init("localhost:8081");
        Graphic.refreshGraphic();
        eventHandler.init(menu, breakpointSection, datetime, connection);
        SimulationState.init();

        //Graphic

        progressbar.init();
        toaster.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toaster.info('Simulation On Play', 'New Simulation State!');
        // Sensor modal
        $("#selectable-sensor").selectable();
    });


});