define(["connection/Request", "TemplateHelper" ], function (request, templateHelper) {
    var ObjectsEventHandler = {};

    ObjectsEventHandler.init = function(connection){
        request.init("GET", {}, "/RootObjects");
        connection.send(request, addNewObjects);

        function addNewObjects(objects) {
            templateHelper.render("objects-selector", JSON.parse(objects), function(template){
                $("#modal-objects-container").append(addEventToAddMoreObjects($(template)))
            });
        }

        function addEventToAddMoreObjects(element) {element.
            on("change", function () {
                element.nextAll().remove();
                if(element.val()!="--") {
                    var id = element.prop("data-id");
                    getObjectSensors(id);
                    request.init("GET", {}, "/ObjectComponents?objectId=" + id);
                    connection.send(request, addNewObjects);
                }
            });
            return element;
        }

        function getObjectSensors(objectId) {
            request.init("GET", {}, "/MesurableAttributes?objectId="+ objectId);
            connection.send(request, showSensors);
        }

        function showSensors(sensors) {
            templateHelper.render("selection", JSON.parse(sensors), function(out){
                $("#selectable-sensor").empty();
                $("#selectable-sensor").append(out);
            });
        }
    };

    return ObjectsEventHandler;

});