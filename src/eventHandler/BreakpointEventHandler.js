define(["connection/Request"], function (request) {
    var BreakpointEventHandler = {};

    BreakpointEventHandler.init = function(breakpointSection, connection){
        $('#add-breakpoint').on("click", function(){
            request.init("POST", {time:breakpointSection.getTimeSelected()}, "/Breakpoint");
            connection.send(request, function(){addBreakpoint()});
        });

        function addBreakpoint(){
            (breakpointSection.isTimeSelected())? breakpointSection.createBreakpointView() : null ;
        }
        //TODO falta añadir el id del brakpoint
        $(".close-breakpoint").on("click", function(){
            var breakpointSection = $(this).parent();
            request.init("DELETE", {breakpointId:"idAhi"}, "/Breakpoint");
            connection.send(request, function(){
                breakpointSection.remove()
            });
        });
    };

    return BreakpointEventHandler;

});