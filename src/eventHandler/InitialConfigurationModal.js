define(['connection/Request', 'connection/Connection','TemplateHelper', 'ClientState'], function (Request, Connection, TemplateHelper, ClientState) {
    InitialConfiguration = {events : {}};


    InitialConfiguration.init = function (websocket) {
        settings();
        getAllSimulations();
        return this;
    };

    InitialConfiguration.show = function () {
        $('#myModal').modal('show');
        return this;
    };

    InitialConfiguration.done = function (onDone) {
        this.events.onDone = function () {
            $('#myModal').modal('hide');
            onDone();
        }
    };

    function configureModal() {
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    }

    function configureButtonClick() {
        $("#SubscriptionButton").on('click', function () {
            if (isAllNecessaryData()) {
                subscribeToSimulation();
            } else {
                alert("Please fill all information")
            }
        });
    }

    function settings() {
        configureModal();
        configureButtonClick();
    }

    function getAllSimulations() {
        Request.init('GET', {}, '/Simulations');
        Connection.send(Request, function (simulations) {
            TemplateHelper.render("selection", JSON.parse(simulations), addSimulations)
        });
    }

    function subscribeToSimulation() {
        ClientState.simulationId = getSelectedSimulation();
        ClientState.username = $("#username").val();
        Connection.setSimulationId(getSelectedSimulation());
        Request.init('POST', {username: $("#username").val()}, '/Subscribe');
        Connection.send(Request, function () {
            InitialConfiguration.events.onDone();
        });
    }

    function addSimulations(template) {
        $('#selectable-simulation').append($(template)).selectable();
    }

    function getSelectedSimulation() {
        var sensorSelection = $("#selectable-simulation").find(".ui-selected");
        if(sensorSelection.length==1)
            return sensorSelection.text();
        return "";
    }

    function isAllNecessaryData() {
        return (getSelectedSimulation() != "") && ($("#username").val() != "");
    }

    return InitialConfiguration;
});