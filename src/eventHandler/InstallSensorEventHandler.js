define(["connection/Request", "TemplateHelper", "Graphic", "ClientState", "SensorsManager"], function (request, TemplateHelper, Graphic, ClientState, SensorsManager) {

        var SensorHandler = {};
        var selectsObject = {};

        function getSelectedSensor() {
            var sensorSelection = $("#selectable-sensor").find(".ui-selected");
            if(sensorSelection.length==1)
                return sensorSelection.text();

        }

        function isAllDataFilled() {
            var sensorSelection = $("#selectable-sensor").find(".ui-selected");
            return ($(selectsObject[0]).val()!="--")  && (sensorSelection.length==1);
        }
        function getLastObjectId() {
            return $(selectsObject[selectsObject.length-2]).find(":selected").attr("data-id");
        }

        function showSensorInstalled(sensor, color) {
            sensor.path = "";
            selectsObject.each(function (index, element) {
                if($(element).val()!="--")
                    sensor.path = sensor.path + "/" +$(element).val();
            });

            TemplateHelper.render("sensor-showing", sensor, function (rendered) {
                $("#installed-sensor-container").append($(rendered)).find("input").keypress(function (event) {
                    if(event.which == 13) {
                        var newSensorValue = {
                            objectId: sensor.objectId,
                            value: parseInt($(this).val()),
                            measurableAttributeName: sensor.measurableAttributeName
                        };
                        request.init("POST", newSensorValue, "/Change");
                        SensorHandler.connection.send(request, function () {

                        });
                    }
                });
                SensorsManager.setSensorColor(sensor, color);
            });
        }

        function installSensor() {
            selectsObject = $("#modal-objects-container").children();
            if(isAllDataFilled()) {
                var sensor = {username:ClientState.username, objectId: getLastObjectId(), measurableAttributeName: getSelectedSensor()};
                request.init("POST", sensor, "/Sensor");
                SensorHandler.connection.send(request, function () {
                    var colorOnGraphic = Graphic.addNewSensor(sensor);
                    showSensorInstalled(sensor, colorOnGraphic);
                    $("#sensorModal").modal("hide");
                });
            }
        }

        SensorHandler.init =  function (connection) {
            this.connection = connection;
            $("#install-sensor-button").on("click", installSensor);
        };

        return SensorHandler;
});