define(function(){
    var DateTimePicker = {datepicker:null};

    //Para milisegundos es con L minuscula
    DateTimePicker.init = function (){
        DateTimePicker.datepicker = $('#example5');

        DateTimePicker.datepicker.datetimepicker({
            showSecond: true,
            showMillisec: true,
            timeFormat: 'hh:mm:ss'
        })
            .timepicker("option", "showAnim", "clip");

    };

    DateTimePicker.showDatePicker = function(){
        DateTimePicker.datepicker.datepicker("show")
    };

    return DateTimePicker;
});