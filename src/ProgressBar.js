define(function () {
    ProgressBar = {};

    ProgressBar.init = function() {
        $("#progressbar").progressbar({
            value: false
        });

        $("#inner-progressbar").progressbar({
            value: 50
        });
    };

    return ProgressBar;

});