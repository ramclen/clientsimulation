define(function(){
    var MenuSection = {};

    MenuSection.toggle = function ($, target) {
        var clickElement = ($(target).prop('tagName')=='A')? $(target):$(target).find("a");
        $(clickElement.attr('href')).collapse('toggle');
    };

    return MenuSection;

});