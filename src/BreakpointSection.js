define(function(){
    BreakpointSection= {};

    BreakpointSection.createBreakpointView = function () {
        breakpoint = $("#example5").clone().attr("id", "");
        breakpointContainer = $('.breakpoint-slot').clone(true, true).removeClass("breakpoint-slot").css('visibility', 'visible');
        timeslot = breakpointContainer.find(".time-slot").append(breakpoint);
        $('#breakpoint-body').append(breakpointContainer);
    };

    BreakpointSection.isTimeSelected = function () {
        return this.getTimeSelected() != "";
    };

    BreakpointSection.getTimeSelected = function () {
        return $("#example5").val();
    }

    return BreakpointSection;
});

