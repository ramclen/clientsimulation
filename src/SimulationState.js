define(["connection/Request", "connection/Connection"], function (Request, Connection) { var SimulationState = {};

    SimulationState.init  = function () {
        $("#play-pause-button").on('click', playPauseAction);
        $("#stop-button").on('click', stopAction);
    };

    function playPauseAction() {
        Request.init("POST", {}, ($(this).hasClass("fa-pause"))? "/Pause" : "/Play");
        Connection.send(Request, function () {
            $("#play-pause-button").toggleClass("fa-pause");
        });
    }

    function stopAction() {
        Request.init("POST", {}, "/Stop");
        Connection.send(Request, function () {
            $("#play-pause-button").removeClass("fa-pause");
        });
    }

    return SimulationState;
});