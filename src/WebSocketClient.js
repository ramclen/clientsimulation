define([ "Graphic", "ClientState", "SensorsManager"], function (Graphic, ClientState, SensorsManager) { var WebSocketServer = {};
    WebSocketServer.init = function (URL) {
        this.webSocket = new WebSocket("ws://" + URL);
        configureWebSocket(this.webSocket);
    };

    function configureWebSocket(websocket) {
        websocket.onopen = function () {
            this.send(ClientState.username+":"+ClientState.simulationId);
        };

        websocket.onmessage = function (message) {message = JSON.parse(message.data);

            if(message.type == "initGraphic")
                Graphic.initTimeLine(message.info);
            if(message.type == "sensorValues") {
                Graphic.addNewData(message.info);
                SensorsManager.update(message.info);
            }
            if(message.type == "breakpointAlert") {
                Graphic.showAlert("stopped by breakpoint");
                SensorsManager.enableAllInputs();
            }
        }
    }

    return WebSocketServer;

});